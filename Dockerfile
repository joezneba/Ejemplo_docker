# Imagen Base 
FROM node:slim
#descripcion de la imagen
LABEL Author="Jonathan Neira"
#creacion de variables de entorno
ENV NODEAPP=/nodeapp
#crear carpteta
RUN mkdir ${NODEAPP}
#correr comandos con la palabra RUN

#definir directotio actual
WORKDIR ${NODEAPP}
#copiar
COPY package.json ${NODEAPP}/
#instalar dependencias 
RUN npm install 
#copia desde el host el directorip actual al contenedor
COPY . ${NODEAPP}
#expone el puerto 8000 del contenedor
EXPOSE 8080
#define el comando final que se ejecutara 
CMD ["npm","start"]
#puerto 8080:8080
#primer parametro del computador , segundo del contenedor de la imagen

#EJECUTAR CONTENEDOR
#CMD ["npm","start"]